import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sua_formatura/database/app_database.dart';
import 'package:sua_formatura/models/contact.dart';
import 'package:sua_formatura/screens/dashboard.dart';

void main() {
  runApp(Teste());
  save(Contact(0, 'alex', 1000)).then((id) {
    // findAll().then((contacts) => debugPrint(contacts.toString()));
  });
  debugPrint("oi");
}

class Teste extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green[900],
        accentColor: Colors.blueAccent[700],
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.blueAccent[700],
          textTheme: ButtonTextTheme.primary,
        ),
      ),
      home: Dashboard(),
    );
  }
}
