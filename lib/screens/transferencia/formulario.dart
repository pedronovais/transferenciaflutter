import 'package:flutter/material.dart';
import 'package:sua_formatura/components/editor.dart';
import 'package:sua_formatura/models/transferencia.dart';

const _tituloAppBar = 'Criando Transferência';

const _rotuloCampoValor = 'Valor';
const _dicaCampoValor = '0.00';

const _rotuloCampoNumeroConta = 'Número da Conta';
const _dicaCampoNumeroConta = '000000';

const _textoBotaoConfirmar = 'Confirmar';

class FormularioTransferencia extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FormularioTransferenciaState();
  }
}

class FormularioTransferenciaState extends State<FormularioTransferencia> {
  final TextEditingController controladorCampoNumeroConta =
      TextEditingController();
  final TextEditingController controladorCampoValor = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(_tituloAppBar),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Editor(
                controlador: controladorCampoNumeroConta,
                dica: _dicaCampoNumeroConta,
                rotulo: _rotuloCampoNumeroConta,
                icone: null),
            Editor(
              controlador: controladorCampoValor,
              dica: _dicaCampoValor,
              rotulo: _rotuloCampoValor,
              icone: Icons.monetization_on,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(_textoBotaoConfirmar),
                ),
                onPressed: () => _criaTransferencia(context),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _criaTransferencia(BuildContext context) {
    debugPrint('clicou no confirmar');
    final int numeroConta = int.tryParse(controladorCampoNumeroConta.text);
    final double valor = double.tryParse(controladorCampoValor.text);
    if (numeroConta != null && valor != null) {
      final transferenciaCriada = Transferencia(valor, numeroConta);
      debugPrint('$transferenciaCriada');
      debugPrint('Criou');
      Navigator.pop(context, transferenciaCriada);
    } else
      debugPrint("Erro");
  }
}
